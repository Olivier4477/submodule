package com.projet.mcb.Controller;

import com.projet.mcb.Foo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/foo")
public class FooController {

    @Autowired
    Foo foo;

    @GetMapping("add")
    public void addCountry() {
        foo.foo2();
    }
}

package com.projet.mcb;



import com.submodule.commun.modelandjpa.Model.Country;
import com.submodule.commun.modelandjpa.Repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;


@SpringBootApplication(scanBasePackages = "com.submodule.commun")

@Component
public class Foo {


    @Autowired
    CountryRepository countryRepository;

    public void foo2() {
        Country country = new Country();
        country.setName("foo 2");
        countryRepository.save(country);
    }

}

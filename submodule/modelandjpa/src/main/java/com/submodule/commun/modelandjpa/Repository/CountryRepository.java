package com.submodule.commun.modelandjpa.Repository;

import com.submodule.commun.modelandjpa.Model.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country, Long> {
}

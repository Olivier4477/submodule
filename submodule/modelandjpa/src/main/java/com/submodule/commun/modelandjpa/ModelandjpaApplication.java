package com.submodule.commun.modelandjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

@Service
@EnableConfigurationProperties
@SpringBootApplication
public class ModelandjpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(ModelandjpaApplication.class, args);
    }

}

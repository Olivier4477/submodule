package com.projet.microservicea;



import com.submodule.commun.modelandjpa.Model.Country;
import com.submodule.commun.modelandjpa.Repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@Component
@SpringBootApplication(scanBasePackages = "com.submodule.commun.modelandjpa")
public class Test {

    @Autowired
    CountryRepository countryRepository;

    public void foo() {
        Country country = new Country();
        country.setName("for test");
        countryRepository.save(country);
    }

}
